#include "demo.h"
#include "ui_demo.h"
#include "colorbutton.h"

#include <QApplication>

#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>

#include <QFileDialog>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->splitter->setStretchFactor(0, 1);
    ui->splitter->setStretchFactor(1, 4);
    ui->splitter->setStretchFactor(2, 1);

    ui->splitter_2->setStretchFactor(0, 3);
    ui->splitter_2->setStretchFactor(1, 1);

    QToolBar * colorPage = new QToolBar;
    addColorButtons (colorPage);
    ui->palette->addTab (colorPage, "Colors");
    ui->palette->setCurrentWidget(colorPage);

    scene = new QGraphicsScene;
    ui->graphicsView->setScene (scene);
    scene->setSceneRect(0, 0, 800, 600);
    // scene->addLine(0, 0, 100, 200, QColor ("red"));

    openFile ("../demo/abc.xml");
}

QString getString (QXmlStreamAttributes & a, QString name)
{
    QString result = "";
    if (a.hasAttribute (name))
    {
        result = a.value(name).toString();
    }
    return result;
}

int getNumber (QXmlStreamAttributes & a, QString name, int init = 0)
{
    int result = init;
    if (a.hasAttribute (name))
    {
        bool ok;
        result = a.value(name).toInt(&ok);
        if (! ok)
            result = init;
    }
    return result;
}

QColor getColor (QXmlStreamAttributes & a, QString name, QColor init = QColor ("yellow"))
{
    QColor result = init;
    if (a.hasAttribute (name))
    {
        QString s = a.value(name).toString ();
        result = QColor (s);
    }
    return result;
}

QString PenToString (QPen p)
{
    return p.color().name();
}

QString BrushToString (QBrush b)
{
    return b.color().name();
}

void MainWindow::on_action_Open_triggered()
{
    QString fileName = QFileDialog::getOpenFileName (this, "Open file");
    if (fileName != "")
        openFile (fileName);
}

void MainWindow::openFile (QString fileName)
{
    QFile f (fileName);
    if (f.open (QFile::ReadOnly))
    {
        QXmlStreamReader r (&f);
        while (! r.atEnd())
        {
            if (r.isStartElement() && r.name().toString() == "node")
            {
                QXmlStreamAttributes a = r.attributes();
                QGraphicsEllipseItem * e = new QGraphicsEllipseItem;

                QString name = getString (a, "name");
                e->setToolTip(name);

                int x = getNumber (a, "x");
                int y = getNumber (a, "y");
                int w = getNumber (a, "w", 100);
                int h = getNumber (a, "h", 80);

                e->setPos (x, y);
                e->setRect (0, 0, w, h);

                QColor c = getColor(a, "pen", QColor ("red"));
                QColor d = getColor(a, "brush", QColor ("yellow"));

                e->setPen(c);
                e->setBrush(d);

                e->setFlag (QGraphicsItem::ItemIsMovable);
                scene->addItem(e);
            }
            r.readNext();
        }
    }
    refreshTree ();
}

void MainWindow::refreshTree()
{
    ui->tree->clear();
    QTreeWidgetItem * top = new QTreeWidgetItem ();
    top->setText(0, "data");

    for (QGraphicsItem * item : scene->items(Qt::AscendingOrder))
    {
        TreeNode * node = new TreeNode ();
        node->setText (0, item->toolTip());
        node->item = item;
        top->addChild (node);
    }

    ui->tree->addTopLevelItem (top);
    ui->tree->expandAll();
}

QTableWidgetItem * MainWindow::addTableLine(QString name, QString value)
{
    int cnt = ui->prop->rowCount();
    ui->prop->setRowCount(cnt+1);

    QTableWidgetItem * cell = new QTableWidgetItem;
    cell->setText (name);
    ui->prop->setItem (cnt, 0, cell);

    cell = new QTableWidgetItem;
    cell->setText (value);
    ui->prop->setItem (cnt, 1, cell);

    return cell;
}

void MainWindow::addColorLine (QString name, QColor value)
{
    QTableWidgetItem * cell = addTableLine (name, value.name());
    cell->setData (Qt::DecorationRole, value);
}

void MainWindow::refreshTable(QGraphicsItem * item)
{
    ui->prop->setRowCount(0);
    ui->prop->setColumnCount(2);
    QStringList labels;
    labels << "Name" << "Value";
    ui->prop->setHorizontalHeaderLabels (labels);

    addTableLine ("tooltip", item->toolTip());
    addTableLine ("x", QString::number (item->x()));
    addTableLine ("y", QString::number (item->y()));

    QAbstractGraphicsShapeItem * shape =
            dynamic_cast <QAbstractGraphicsShapeItem *> (item);
    if (shape != nullptr)
    {
        addColorLine ("pen", shape->pen().color() );
        addColorLine ("brush", shape->brush().color() );
    }
}

void MainWindow::on_tree_itemDoubleClicked(QTreeWidgetItem * node, int column)
{
    ui->prop->setRowCount(0);
    TreeNode * tree_node = dynamic_cast <TreeNode *> (node);
    if (tree_node != nullptr && tree_node->item != nullptr)
       refreshTable (tree_node->item);
}

void MainWindow::on_action_Save_triggered()
{
    QString fileName = QFileDialog::getSaveFileName (this, "Save file");
    if (fileName != "")
    {
        setWindowTitle (fileName);
        QFile f (fileName);
        if (f.open (QFile::WriteOnly))
        {
            QXmlStreamWriter w (&f);
            w.setAutoFormatting (true);
            w.writeStartDocument();
            w.writeStartElement("data");

            for (QGraphicsItem * item : scene->items() )
            {
               QGraphicsEllipseItem * e =
                       dynamic_cast < QGraphicsEllipseItem * > (item) ;
               if (e != nullptr)
               {
                  w.writeStartElement("node");
                  w.writeAttribute ("name", e->toolTip());
                  w.writeAttribute ("pen", PenToString (e->pen()));
                  w.writeAttribute ("brush", BrushToString (e->brush()));
                  w.writeAttribute ("x", QString::number ( e->x() ));
                  w.writeAttribute ("y", QString::number ( e->y() ));
                  w.writeAttribute ("width", QString::number ( e->rect().width() ));
                  w.writeAttribute ("height", QString::number ( e->rect().height() ));
                  w.writeEndElement();
               }
            }

            w.writeEndElement(); // end of data
            w.writeEndDocument();
        }
    }
}

void MainWindow::on_action_Run_triggered()
{
   QGraphicsEllipseItem * item = new QGraphicsEllipseItem;
   item->setRect(0, 0, 100, 80);
   item->setPos(100, 100);
   item->setPen (QColor ("cornflowerblue"));
   item->setBrush (QColor ("yellow"));
   item->setToolTip("elipsa");
   item->setFlag(QGraphicsItem::ItemIsMovable);
   scene->addItem (item);
}

void MainWindow::on_action_Quit_triggered()
{
   close ();
}

MainWindow::~MainWindow()
{
    delete ui;
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
