QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

SOURCES += \
    mainwindow.cc

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui

