#include "db.h"
#include "io.h"

#include <QApplication>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlTableModel>
#include <QSqlError>

#include <QGraphicsItem>
#include <QTextCharFormat>

/* ---------------------------------------------------------------------- */

DbView::DbView (QWidget * parent,
                QGraphicsScene * scene_param,
                QTreeWidget * tree_param,
                QTextEdit * info_param) :
   QTableView (parent),
   scene (scene_param),
   tree (tree_param),
   info (info_param)
{
    example ();
}

/* ---------------------------------------------------------------------- */

void DbView::message (QSqlError err)
{
    QColor save = info->textColor ();
    info->setTextColor (QColor ("red"));
    info->append ("Error:");
    info->setTextColor (QColor ("blue"));
    info->append (err.text ());
    info->setTextColor (save);
}

/* ---------------------------------------------------------------------- */

void DbView::example ()
{
   QSqlDatabase db = QSqlDatabase::addDatabase ("QSQLITE");
   // db.setDatabaseName (":memory:");
   db.setDatabaseName ("test.sqlite");

   // dnf install qt5-qtbase-mysql
   // QSqlDatabase db = QSqlDatabase::addDatabase ("QMYSQL");

   // dnf install qt5-qtbase-postgresql
   // QSqlDatabase db = QSqlDatabase::addDatabase ("QPSQL");

   // db.setHostName ("domain.name");
   // db.setUserName ("user");
   // db.setPassword ("password");
   // db.setDatabaseName ("user");

   bool ok = db.open ();
   if (! ok)
   {
      message (db.lastError());
      return;
   }

   db.exec ("DROP TABLE IF EXISTS shapes");

   db.exec ("CREATE TABLE shapes (name TEXT, type TEXT, "
                 "x INTEGER, y INTEGER, w INTEGER, h INTEGER, "
                 "pen TEXT, brush TEXT)");

   // db.exec ("INSERT INTO colors (name, red, green, blue) VALUES (\"blue\", 0, 0, 255)");

   QSqlQuery insert (db);
   ok = insert.prepare ("INSERT INTO shapes (name, type, x, y, w, h, pen, brush) "
                                   "VALUES (:name, :type, :x, :y, :w, :h, :pen, :brush)");
   if (!ok) message (insert.lastError ());

   for (QGraphicsItem * item : scene->items (Qt::AscendingOrder))
   {
       insert.bindValue (":name", item->toolTip ());
       insert.bindValue (":type", itemType (item));

       int x = item->x();
       int y = item->y();
       insert.bindValue (":x", x);
       insert.bindValue (":y", y);

       int w = 0;
       int h = 0;
       QString pen = "";
       QString brush = "";

       if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
       {
          pen=  penToString (shape->pen());
          brush = brushToString (shape->brush());

           if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
           {
              QRectF r = e->rect ();
              w = r.width ();
              h = r.height();
           }

           if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
           {
               QRectF r = e->rect ();
               w = r.width ();
               h = r.height();
           }
       }

       if (QGraphicsLineItem * e = dynamic_cast < QGraphicsLineItem * > (item))
       {
          pen = penToString (e->pen());
          w = e->line().dx();
          w = e->line().dy();
       }

       insert.bindValue (":w", w);
       insert.bindValue (":h", h);
       insert.bindValue (":pen", pen);
       insert.bindValue (":brush", brush);

       ok = insert.exec ();
       if (!ok) message (insert.lastError ());
   }

   QSqlTableModel * model = new QSqlTableModel (this, db);
   model->setTable ("shapes");
   model->select ();
   this->setModel (model);

   QStringList list = db.tables();
   info->append ("tables: " + list.join (","));

   for (int i = 0; i < list.size(); i++)
   {
       QString id = list[i];
       QSqlQuery query = db.exec ("SELECT * FROM " + id);

       QTreeWidgetItem * item = new QTreeWidgetItem;
       item->setText (0, id);
       item->setForeground (0, QColor ("orange"));
       tree->addTopLevelItem (item);

       QSqlRecord rec = query.record();
       for (int k = 0; k < rec.count (); k++)
       {
          QTreeWidgetItem * subitem = new QTreeWidgetItem;
          subitem->setText (0, rec.fieldName (k));
          subitem->setForeground (0, QColor ("blue"));
          item->addChild (subitem);
       }

       tree->expandItem (item);
   }
}

/* ---------------------------------------------------------------------- */
