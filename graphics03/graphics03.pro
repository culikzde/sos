QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    colorbutton.cc \
    demo.cc

HEADERS += \
    colorbutton.h \
    demo.h

FORMS += \
    demo.ui
