#ifndef DEMO_H
#define DEMO_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QTreeWidgetItem>
#include <QTableWidgetItem>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void openFile (QString fileName);
    void refreshTree ();

    QTableWidgetItem * addTableLine (QString name, QString value);
    void addColorLine (QString name, QColor value);
    void refreshTable (QGraphicsItem *item);

private slots:
    void on_action_Open_triggered();
    void on_action_Save_triggered();

    void on_action_Run_triggered();
    void on_action_Quit_triggered();

    void on_tree_itemDoubleClicked(QTreeWidgetItem *item, int column);

private:
    Ui::MainWindow * ui;
    QGraphicsScene * scene;
};

class TreeNode : public QTreeWidgetItem
{
public:
    QGraphicsItem * item;
    TreeNode () : item (nullptr) { }
};

#endif // DEMO_H
