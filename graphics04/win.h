#ifndef WIN_H
#define WIN_H

#include <QGraphicsItem>

class Win // MainWindow interface
{
    public:
        virtual void refreshTree () = 0;
        virtual void refreshProperties (QGraphicsItem * item) = 0;

        virtual void refreshTreeName (QGraphicsItem * item, QString name) = 0;
        virtual void refreshNewTreeItem (QGraphicsItem * item) = 0;
};

#endif // WIN_H
