
/* socket.h */

#ifndef SOCKET_H
#define SOCKET_H

#include <QTcpServer>
#include <QTextEdit>

class SocketReceiver : public QObject
{

   public:
      explicit SocketReceiver (QWidget *parent, int port_param = 1234, QTextEdit * info_param = nullptr);
      ~ SocketReceiver ();

      void run();
   private:
      QTextEdit * info ;

      int port;
      QTcpServer * server;
      QTcpSocket * socket;
      void print (QString s);
      void connection ();
};

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

#endif // SOCKET_H
