Softwarový seminář - Příklady
=============================

Instalace: Qt Creator
---------------------

Windows: http://download.qt.io/official_releases/qt/5.12/5.12.2/

Windows Git: http://git-scm.com/download/win

( Windows Tortoise Git: http://tortoisegit.org/download/ )


Arch Linux: pacman -S qtcreator

Debian/Ubuntu: apt-get install qtcreator

Fedora: dnf install qt-creator

Spustime Qt Creator
-------------------

Qt Creator / Open File Or Project / Import Project / Git Clone

![creator01.png](pictures/creator01.png)

Repository: https://gitlab.fjfi.cvut.cz/culikzde/sos

Path: /home/.../test

Na Windows nefunguji sitove cesty zacinajici // , pouzijte sitovy disk s pismenem (napr M:) a Qt minimalne 5.12.2

![creator02.png](pictures/creator02.png)

Pokud se ve Windows nabizi vice kompilatoru, zkuste MinGW (64-bitu)

![creator03.png](pictures/creator03.png)

Soubor sos.pro urcuje, ktery projekt spustime

![creator04.png](pictures/creator04.png)

Zde je nas priklad

![graphics01.png](pictures/graphics01.png)

