#ifndef XMLIO_H
#define XMLIO_H

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include <QGraphicsScene>
#include <QGraphicsItem>

void readXml (QXmlStreamReader & r, QGraphicsScene * scene,  QGraphicsItem * target = nullptr);

void writeBegin (QXmlStreamWriter & w);
void writeItem (QXmlStreamWriter & w, QGraphicsItem * item);
void writeEnd (QXmlStreamWriter & w);

void writeXml (QXmlStreamWriter & w, QGraphicsScene * scene);

#endif // XMLIO_H
